const form = document.getElementById("film-form");
const titleElement = document.querySelector("#title");
const directorElement = document.querySelector("#director");
const urlElement = document.querySelector("#url");
const cardBody = document.querySelectorAll(".card-body")[1];
const clear = document.getElementById("clear-films");

//Since we write our methods static, we don't need to create methods (ES6)
/* //Starting UI object
const ui = new UI();
//Create storage object
const storage = new Storage(); */

//Loading all events
eventListeners();

function eventListeners(){
    form.addEventListener("submit", addFilm);
    document.addEventListener("DOMContentLoaded", function(){
        let films = Storage.getFilmsFromStorage();
        UI.loadAllFilms(films); //ES6
        //ui.loadAllFilms(films); //we called loadAllFilms methods using ui object in prototype notation
    })

    cardBody.addEventListener("click", deleteFilm);
    clear.addEventListener("click", clearAllFilms);
}

function addFilm(e){
    const title = titleElement.value;
    const director = directorElement.value;
    const url = urlElement.value;

    if(title === "" || director === "" || url === ""){
        UI.displayMessage("Fill in all blanks", "danger")
    }
    else{
        const newFilm = new Film(title, director, url);
        UI.addFilmToUI(newFilm);
        Storage.addFilmToStorage(newFilm);
        UI.displayMessage("Added successfully", "success");
    }

    UI.clearInput(titleElement, directorElement, urlElement);
    e.preventDefault();
}

function deleteFilm(e){
    if(e.target.id === "delete-film"){
        UI.deleteFilmFromUI(e.target);
        //console.log(e.target.parentElement.previousSibling.previousSibling.textContent);
        Storage.deleteFilmFromStorage(e.target.parentElement.previousSibling.previousSibling.textContent);
        UI.displayMessage("Deleted successfully", "success");
    }
}

function clearAllFilms(){
    if(confirm("Are you sure to delete all?")){
        UI.clearAllFilmsFromUI();
        Storage.clearAllFilmsFromStorage();
    }
    
}