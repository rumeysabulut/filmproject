/* function Storage(){

}

Storage.prototype.addFilmToStorage = function(newFilm){
    let films = this.getFilmsFromStorage();

    films.push(newFilm);
    localStorage.setItem("films", JSON.stringify(films));

    
}

Storage.prototype.getFilmsFromStorage = function(){
    //Create an empty array if storage does not have any films,
    //otherwise fetch films from storage and add new film to the array
    //Finally, insert this array to the storage

    let films;

    if(localStorage.getItem("films") === null){
        films = [];
    }
    else{
        films = JSON.parse(localStorage.getItem("films"));
        //Since getItem returns a string, it should be parsed to form the array
    }
    return films;
}

Storage.prototype.deleteFilmFromStorage = function(filmTitle){
    let films = this.getFilmsFromStorage();

    films.forEach(function(film,index){
        if(film.title === filmTitle){
            films.splice(index,1);
        }
    });
    localStorage.setItem("films", JSON.stringify(films));
}

Storage.prototype.clearAllFilmsFromStorage = function(){
    localStorage.removeItem("films");

} */

/* ES6 Standard */

class Storage {

    static addFilmToStorage(newFilm){
        let films = this.getFilmsFromStorage();
    
        films.push(newFilm);
        localStorage.setItem("films", JSON.stringify(films));
    
        
    }
    
    static getFilmsFromStorage(){
        //Create an empty array if storage does not have any films,
        //otherwise fetch films from storage and add new film to the array
        //Finally, insert this array to the storage
    
        let films;
    
        if(localStorage.getItem("films") === null){
            films = [];
        }
        else{
            films = JSON.parse(localStorage.getItem("films"));
            //Since getItem returns a string, it should be parsed to form the array
        }
        return films;
    }
    
    static deleteFilmFromStorage(filmTitle){
        let films = this.getFilmsFromStorage();
    
        films.forEach(function(film,index){
            if(film.title === filmTitle){
                films.splice(index,1);
            }
        });
        localStorage.setItem("films", JSON.stringify(films));
    }
    
    static clearAllFilmsFromStorage(){
        localStorage.removeItem("films");
    
    }

}